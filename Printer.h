#pragma once

#include <thread>
#include <stdio.h>
#include <string>
#include <sstream>
#include <vector>

class Printer
{
public: 
    Printer(bool printThreadId = true)
    {
        if( printThreadId )
        {
            operator<<(std::this_thread::get_id());
            operator<<(":");
        }
    };
    virtual ~Printer()
    {
        finalise();
    };

    template < typename T >
    Printer& operator<<( T value)
    {
        m_stringStream<<value<<m_joiningChar;
        return *this;
    };

private:
    void finalise()
    {
        m_stringStream<<std::endl;
        std::cout<<m_stringStream.str()<<std::flush;
    };

    const char* m_joiningChar = " ";

    std::stringstream m_stringStream;
    std::vector<char*> m_values;
    std::vector<size_t> m_lengths;

};
