#pragma once

#include "Dimentions.h"

#include <ncurses.h>

class  AutoWindow
{
public: 
    AutoWindow( const AutoWindow& other)
        : m_pWindow( other.m_pWindow)
        , m_pCounter( other.m_pCounter)
    {
        ++(*m_pCounter);
    }

    AutoWindow( Dimentions dim)
    {
        m_pWindow = newwin( dim.height, dim.width, dim.y, dim.x);
        m_pCounter = new long int();
        *m_pCounter = 1;
    };

    virtual ~AutoWindow()
    {
        --(*m_pCounter);
        if ( m_pCounter == 0 )
        {
            delwin( m_pWindow );
            delete m_pCounter;
        }
        else if ( m_pCounter < 0 )
        {
            exit(1);
        }
    }

    operator WINDOW*()
    {
        return m_pWindow;
    };

private:

    WINDOW* m_pWindow;
    long int* m_pCounter;
};

