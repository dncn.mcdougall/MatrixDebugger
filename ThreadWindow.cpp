#include "ThreadWindow.h"

#include <algorithm>

ThreadWindow ThreadWindow::createPrinter( const char* line, Dimentions dim )
{
    return ThreadWindow( extractThreadId(line), dim);
}

void ThreadWindow::initialiseWithEmptyLines(size_t lines)
{
    if ( synchronise )
    {
        for ( size_t i = 0; i < lines; ++i )
        {
            addLine(" --- ");
        }
    }
}

bool ThreadWindow::print(const char* line)
{
    std::string threadId = extractThreadId( line );
    if ( threadId == m_threadId )
    {
        std::string strLine(line);
        size_t pos = strLine.find(":");
        if ( pos != std::string::npos )
        {
            addLine(trim(strLine.substr(pos+2)));
        }
        else
        {
            addLine("");
        }

        return true;
    }
    else
    {
        if ( synchronise )
        {
            addLine(" --- ");
        }
        return false;
    }
}

size_t ThreadWindow::lineCount()
{
    return m_lines.size();
}

void ThreadWindow::incOffset()
{
    long int maxOffset = maxHistory - (m_dim.height+2);
    maxOffset = std::min( maxOffset, (long int)(m_lines.size() - (m_dim.height+2) ));
    maxOffset = std::max( maxOffset, 0l);
    
    if ( m_offset < (size_t)maxOffset )
    {
        ++m_offset;
    }
}

void ThreadWindow::decOffset()
{
    if ( m_offset > 0 )
    {
        --m_offset;
    }
}

void ThreadWindow::maxOffset()
{
    long int maxOffset = maxHistory - (m_dim.height+2);
    maxOffset = std::min( maxOffset, (long int)(m_lines.size() - (m_dim.height+2) ));
    maxOffset = std::max( maxOffset, 0l);
    
    m_offset = (size_t) maxOffset;
}

void ThreadWindow::resetOffset()
{
    m_offset = 0;
}

void ThreadWindow::refresh()
{
    box( m_window, 0, 0);
    std::string heading;
    heading.append( m_threadId);
    heading.append(" l:");
    heading.append(std::to_string(m_lines.size()));
    heading.append(" o:");
    heading.append(std::to_string(m_offset));

    mvwaddstr( m_window, 0, 1, heading.c_str());

    size_t width = (m_dim.width-2);
    long int firstLineToPrint = m_lines.size() - (m_dim.height-2);
    if ( firstLineToPrint < 0 )
    {
        firstLineToPrint = 0;
    }
    long int firstLine = firstLineToPrint - m_offset;
    long int lastLine = m_lines.size() - m_offset;
    if ( firstLine < 0 )
    {
        lastLine -= firstLine;
        firstLine = 0;
    }
    size_t line = 1;


    for ( size_t i = firstLine; i < (size_t)lastLine; ++i )
    {
        mvwprintw(m_window, line, 1, "%-*.*s",width,width,m_lines[i].c_str());
        ++line;
    }
    wrefresh( m_window);
}

std::string ThreadWindow::extractThreadId( const char* line)
{
    std::string strLine(line);
    size_t pos = strLine.find(':');
    return trim(strLine.substr(0,pos));
}

std::string ThreadWindow::trim(std::string s)
{
    auto lItt = s.begin();
    while( lItt != s.end())
    {
        if ( std::isspace( *lItt ) )
        {
            ++lItt;
        }
        else
        {
            break;
        }
    }
    s.erase(s.begin(), lItt);

    auto rItt = s.rbegin();
    while( rItt != s.rend())
    {
        if ( std::isspace( *rItt ) )
        {
            ++rItt;
        }
        else
        {
            break;
        }
    }
    s.erase(rItt.base(), s.end());

    return s;
}

ThreadWindow::ThreadWindow( std::string threadId, Dimentions dim)
    : m_threadId(threadId)
    , m_dim(dim)
    , m_window(m_dim)
    , m_offset(0)
{
    // do nothing
}


void ThreadWindow::addLine( std::string line)
{
    m_lines.push_back(line);

    if ( m_lines.size() > maxHistory )
    {
        m_lines.erase(m_lines.begin(), 
                      m_lines.begin() + ( m_lines.size() - maxHistory));
    }

    if ( m_offset != 0 )
    {
        incOffset();
    }
}
