#include <iostream>
#include <stdio.h>
#include <string.h>
#include <limits>
#include <memory>

#include <thread>
#include <chrono>

#include "Printer.h"

void printSomeThings()
{
    std::chrono::seconds sec(1);
    for ( int i = 0; i < 15; ++i )
    {
        Printer()<<"Hello world";
        Printer()<<"A Cat";
        Printer()<<"Another Cat";
        Printer()<<"The cat jumped over the mouse";
        Printer()<<"The lazy brown dog chased the cat";
        Printer()<<"I'm not sure of the context.";
        Printer()<<"Panthers stalk.";
        Printer()<<"Potatoes!!";
        Printer()<<"Boil 'em mash 'em stik 'em in a stew.";

        std::this_thread::sleep_for(sec );
    }
}

int main( int argc, char** argv )
{
    std::thread th1(printSomeThings);
    std::thread th2(printSomeThings);
    std::thread th3(printSomeThings);
    std::thread th4(printSomeThings);
    std::thread th5(printSomeThings);
    std::thread th6(printSomeThings);
    std::thread th7(printSomeThings);
    std::thread th8(printSomeThings);

    th1.join();
    th2.join();
    th3.join();
    th4.join();
    th5.join();
    th6.join();
    th7.join();
    th8.join();
}
