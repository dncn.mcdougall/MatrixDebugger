CXX=g++
FLAGS=-Werror -Wall
LIBS=-pthread -lncurses

DEPENDS=ThreadWindow
OBJECTS=$(foreach object,$(DEPENDS),$(addprefix bin/,$(addsuffix .o,$(object))))

reader.bin: AutoWindow.h Dimentions.h

writer.bin: Printer.h

test: writer.bin reader.bin Makefile
	./writer.bin | ./reader.bin 8

testReader: reader.bin Makefile
	./reader.bin 4

testWriter: writer.bin Makefile
	./writer.bin | less

%.bin: %.cpp ${OBJECTS}
	${CXX} -std=c++11 ${FLAGS} -o $@ $< ${OBJECTS} ${LIBS}

.PRECIOUS: bin/%.o
bin/%.o: %.cpp %.h bin
	${CXX} -std=c++11 ${FLAGS} -c -o $@ $< ${LIBS}

bin:
	mkdir bin

.PHONEY: clean
clean:
	rm -rf ./bin
	rm -f writer.bin
	rm -f reader.bin
