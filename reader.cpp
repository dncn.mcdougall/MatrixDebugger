#include <iostream>

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <vector>
#include <signal.h>
#include <poll.h>

#include <chrono>
#include <thread>

#include <ncurses.h>

#include "ThreadWindow.h"
#include "Dimentions.h"

#define TIMEOUT 50

static void sighandler( int sig)
{
    endwin();
    exit(sig);
}

char* nb_gets(char* buffer, size_t bufferSize)
{
    pollfd desc;
    // desc.fd = stdin;
    desc.fd = STDIN_FILENO;
    desc.events = POLLIN;
    int ret = poll(&desc, 1, TIMEOUT);
    if ( ret > 0 )
    {
        char * value = fgets(buffer, bufferSize, stdin);
        return value;
    }
    else if ( ret < 0 )
    {
        printw("error");
    }
    return nullptr;
}

void goToTop( int ch, int prevCh, std::vector<ThreadWindow>& windows)
{
    if ( ( ch == 'g' && prevCh == 'g' ) || ch == KEY_HOME )
    {
        for( ThreadWindow& window: windows)
        {
            window.maxOffset();
            window.refresh();
        }
    }
}

void goUp( int ch, int prevCh, std::vector<ThreadWindow>& windows)
{
    if( ch == 'k' || ch == KEY_UP )
    {
        for( ThreadWindow& window: windows)
        {
            window.incOffset();
            window.refresh();
        }
    }
}

void goToBottom( int ch, int prevCh, std::vector<ThreadWindow>& windows)
{
    if( ch == 'G' || ch == KEY_END )
    {
        for( ThreadWindow& window: windows)
        {
            window.resetOffset();
            window.refresh();
        }
    }
}

void goDown( int ch, int prevCh, std::vector<ThreadWindow>& windows)
{
    if( ch == 'j' || ch == KEY_DOWN )
    {
        for( ThreadWindow& window: windows)
        {
            window.decOffset();
            window.refresh();
        }
    }
}

bool shouldExit( int ch, int prevCh)
{
    if( ch == 'q' && prevCh == ':' )
    {
        return true;
    }
    return false;
}

int main( int argc, char** argv )
{

    signal( SIGINT, sighandler);
    try
    {
        size_t numberOfThreads;
        if ( argc < 2 )
        {
            numberOfThreads = 1;
        } 
        else 
        {
            numberOfThreads = std::stoul(argv[1]);
        }

        const char* term_type = getenv("TERM");
        if ( term_type == nullptr || *term_type == '\0')
        {
            term_type = "unknown";
        }
        FILE* term_in = fopen("/dev/tty","r");
        if ( term_in == nullptr )
        {
            exit(1);
        }

        SCREEN* pScreen = newterm(term_type, stdout, term_in);
        set_term(pScreen);
        // initscr();
        cbreak();
        noecho();
        timeout(TIMEOUT);
        keypad(stdscr, true);


        std::vector<ThreadWindow> windows;

        int ch = ERR;
        int prevCh = ERR;
        std::chrono::milliseconds timeOut(500);
        auto lastTime = std::chrono::system_clock::now();

        size_t bufferSize = 1000;
        char* buffer = new char[bufferSize];
        char* returnedStr = nb_gets(buffer, bufferSize);
        while ( true )
        {
            if ( returnedStr != nullptr )
            {
                bool printed = false;
                for( ThreadWindow& window: windows)
                {
                    if ( window.print( buffer) )
                    {
                        printed = true;
                        if ( ! ThreadWindow::synchronise )
                        {
                            break;
                        }
                    }
                }
                if ( !printed )
                {
                    Dimentions dim;
                    dim.width = (COLS/numberOfThreads+1);
                    dim.height = LINES;
                    dim.x = windows.size()*(dim.width -1);
                    dim.y = 0;

                    // dim.width = COLS;
                    // dim.height = LINES/numberOfThreads;
                    // dim.x = 0;
                    // dim.y = windows.size()*dim.height;

                    ThreadWindow tmp =  ThreadWindow::createPrinter( buffer, dim );
                    if ( windows.size() > 0 )
                    {
                        tmp.initialiseWithEmptyLines( windows[0].lineCount() -1 );
                    }
                    windows.push_back(tmp);
                    windows.back().print(returnedStr);
                }
                for( ThreadWindow& window: windows)
                {
                    window.refresh();
                }
            }
            returnedStr = nb_gets(buffer, bufferSize);
            refresh();

            ch = getch();
            if ( ch != ERR )
            {
                lastTime = std::chrono::system_clock::now();
                goUp(ch, prevCh, windows);
                goDown(ch, prevCh, windows);
                goToTop(ch, prevCh, windows);
                goToBottom(ch, prevCh, windows);
                if ( shouldExit(ch, prevCh) )
                {
                    break;
                }
                prevCh = ch;
            }
            else if ( prevCh != ERR && (std::chrono::system_clock::now() - lastTime) > timeOut )
            {
                prevCh = ERR;
            }
        }


        endwin();

        delete[] buffer;

    } 
    catch (std::exception& exc)
    {
        std::cout<<"Exception: "<<exc.what()<<std::endl;
    }
    return 0;
}
