#pragma once

#include <iostream>
#include <string.h>
#include <vector>

#include <ncurses.h>

#include "Dimentions.h"
#include "AutoWindow.h"

class ThreadWindow
{
public:

    static const size_t maxHistory = 1000;
    static const bool synchronise = true;

    static ThreadWindow createPrinter( const char* line, Dimentions dim );
    void initialiseWithEmptyLines(size_t lines);

    bool print(const char* line);

    size_t lineCount();

    void incOffset();
    void decOffset();
    void maxOffset();
    void resetOffset();

    void refresh();

    static std::string trim(std::string s);

private:

    static std::string extractThreadId( const char* line);

    ThreadWindow( std::string threadId, Dimentions dim);

    void addLine( std::string str);

    std::string m_threadId;
    Dimentions m_dim;
    AutoWindow m_window;

    size_t m_offset;
    std::vector<std::string> m_lines;

};
